﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace EdgeStreamer
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public ObservableCollection<string> Favorites { get; private set; } = new ObservableCollection<string>();

        public MainPage()
        {
            this.InitializeComponent();
        }

        private void SiteSelectionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var uri = new Uri((sender as ComboBox).SelectedItem.ToString());

            try
            {
                webView1.Navigate(uri);
            }
            catch (Exception)
            {
            }
        }

        private void SiteSelectionComboBox_TextSubmitted(ComboBox sender, ComboBoxTextSubmittedEventArgs args)
        {

        }

        private void ConfigButton_Click(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(SitesConfig));
        }

        private void webView1_ContainsFullScreenElementChanged(WebView sender, object args)
        {
            var view = ApplicationView.GetForCurrentView();

            if (view.IsFullScreenMode)
            {
                view.ExitFullScreenMode();
            }
            else
            {
                view.TryEnterFullScreenMode();
            }

            if (view.IsFullScreenMode)
            {
                SiteSelectionComboBox.Visibility = Visibility.Collapsed;
                ConfigButton.Visibility = Visibility.Collapsed;
                BackButton.Visibility = Visibility.Collapsed;
            }
            else
            {
                SiteSelectionComboBox.Visibility = Visibility.Visible;
                ConfigButton.Visibility = Visibility.Visible;
                BackButton.Visibility = Visibility.Visible;
            }
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var content = await App.GetConfigString();
            var sites = content.Split(",").ToList();
            foreach (var site in sites)
            {
                Favorites.Add(site.TrimStart());
            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (webView1.CanGoBack)
            {
                webView1.GoBack();
            }
        }

        private void webView1_ContentLoading(WebView sender, WebViewContentLoadingEventArgs args)
        {
            StatusTextBlock.Text = $"Loading {args.Uri}";
        }

        private void webView1_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            StatusTextBlock.Visibility = Visibility.Visible;
            StatusTextBlock.Text = $"Navigating to {args.Uri}";
        }

        private void webView1_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            StatusTextBlock.Visibility = Visibility.Collapsed;
            StatusTextBlock.Text = string.Empty;
        }

        
    }
}
